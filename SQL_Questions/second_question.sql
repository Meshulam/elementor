SELECT CONCAT(ROUND((c.visitors / d.visitors) * 100), '%') as visitors_precent, c.site FROM
(SELECT SUM(number_of_visitors) as visitors, site FROM
(SELECT a.visit_date, a.site, a.number_of_visitors FROM SITE_VISITORS a
JOIN 
(SELECT start_date, end_date, site FROM PROMOTION_DATES) b ON b.site = a.site AND a.visit_date >= b.start_date AND a.visit_date <= b.end_date)
GROUP BY site) c 
INNER JOIN 
(SELECT SUM(number_of_visitors) as visitors, site FROM SITE_VISITORS GROUP BY site) d ON c.site = d.site