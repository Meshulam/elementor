SELECT a.department_id, a.salary as top_salary, a.salary - b.salary as salary_diff FROM
(SELECT department_id, MAX(salary) as salary, FROM EMPLOYEES GROUP BY department_id) a
JOIN
(SELECT department_id, MAX(salary), as salary, FROM EMPLOYEES
WHERE (department_id, salary) not in (SELECT department_id, MAX(salary) as salary FROM EMPLOYEES GROUP BY department_id)
GROUP BY department_id) b
ON (a,department_id = b.department_id);