from pymongo import MongoClient
from datetime import datetime

class Dal:

    def __init__(self):
        self.client = MongoClient(
            "mongodb+srv://roeemeshulam:I7S85po91@black-bird.jmtkf.mongodb.net/Elemntor?retryWrites=true&w=majority")
        db = self.client['Elmentor']
        self.collection = db['urls_analysis']

    def get_updated_urls_analysis(self, urls):
        results = self.collection.find({'url':{'$in':urls}})
        filtered_res = [res for res in results if Dal.time_diff(res['timestamp'])]
        return filtered_res

    @staticmethod
    def time_diff(timestamp):
        timedelta = ((datetime.now() - timestamp).seconds // 60) % 60
        return timedelta < 30

    def insert_url_analysis(self, url, is_risky, total_votes, categories):
        analysis_dict = {'url': url, 'site_risky': is_risky, 'total_voting': total_votes, 'categories': categories,
                         'timestamp': datetime.now()}
        self.collection.update_one({'url': url},{'$set':analysis_dict}, upsert=True)

if __name__ == '__main__':
    dal = Dal()
    urls = ['fourthgate.org/Yryzvt', 'www.elementor.com']
    r = dal.get_updated_urls_analysis(urls)