import csv
import uvicorn
from fastapi import FastAPI, File, UploadFile
from controllers.virus_analysis import VirusScanner

import aiofiles

app = FastAPI()

@app.post("/")
async def create_upload_file(file: UploadFile = File(...)):
    filepath = f'./data/{file.filename}'
    async with aiofiles.open(filepath, 'wb') as out_file:
        content = await file.read()  # async read
        await out_file.write(content)

    with open(filepath) as f:
        reader = csv.reader(f)
        urls_list = list(reader)
        flat_list = [item for sublist in urls_list for item in sublist]
        update_urls(flat_list)
    return {"Result": "OK"}

def update_urls(urls_list):
    scanner = VirusScanner()
    scanner.get_and_update_urls(urls_list)


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)
#     v = VirusScanner()
#     url = 'www.elementor.com'
#     analyze_id = v.analyze_url(url)
#     url_id = v.get_url_id(analyze_id)
#     v.get_url_data(url_id, url)
