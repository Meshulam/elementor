from collections import Counter
from dal import Dal
import requests

class VirusScanner:
    def __init__(self):
        self.dal = Dal()

    def get_and_update_urls(self, urls_list):
        updated_urls = self.dal.get_updated_urls_analysis(urls_list)
        need_update = [url for url in urls_list if url not in updated_urls]
        for url in need_update:
            analysis_id = self.get_analysis_id(url)
            url_id = self.get_url_id(analysis_id)
            self.insert_url_data(url_id, url)



    def get_analysis_id(self, url):
        headers = {'x-apikey': '3d6d1b81d566de0070c2579f3269c497ccfd0bd7a06a774f0766feb91ba2885f',
                   'Content-Type': 'application/x-www-form-urlencoded'}
        payload = {'url': url}
        r = requests.post('https://www.virustotal.com/api/v3/urls', data=payload, headers=headers)
        return r.json()['data']['id']

    def get_url_id(self, analysis_id):
        headers = {'x-apikey': '3d6d1b81d566de0070c2579f3269c497ccfd0bd7a06a774f0766feb91ba2885f'}
        r = requests.get(f'https://www.virustotal.com/api/v3/analyses/{analysis_id}', headers=headers)
        return r.json()['meta']['url_info']['id']

    def insert_url_data(self, url_id, url):
        headers = {'x-apikey': '3d6d1b81d566de0070c2579f3269c497ccfd0bd7a06a774f0766feb91ba2885f'}
        r = requests.get(f'https://www.virustotal.com/api/v3/urls/{url_id}', headers=headers)
        data = r.json()['data']
        mapped_results = VirusScanner.get_mapped_results(data)
        is_risky = mapped_results['malicious'] > 1 or mapped_results['malware'] > 1 or mapped_results['phishing'] > 1
        categories = data['attributes']['categories']
        mapped_caegories = Counter(categories.values())
        self.dal.insert_url_analysis(url, is_risky, mapped_results, mapped_caegories)


    @staticmethod
    def get_mapped_results(data):
        results = data['attributes']['last_analysis_results']
        mapped_results = [result['result'] for result in results.values()]
        count_results = Counter(mapped_results)
        return count_results